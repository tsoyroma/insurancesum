package kz.alfabank.creditutils.rest;

import kz.alfabank.creditutils.domain.InsuranceDictioanry;
import kz.alfabank.creditutils.domain.InsuranceSum;
import kz.alfabank.creditutils.domain.Response;
import kz.alfabank.creditutils.service.InsuranceServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("/calc")
public class InsCalcResource {

    private static final Logger LOG = LoggerFactory.getLogger(InsCalcResource.class);

    private static final String DEFAULT_PRODUCE_MEDIA_TYPE = MediaType.APPLICATION_JSON_VALUE;

    private final InsuranceServiceImpl insuranceService;

    @Autowired
    public InsCalcResource(InsuranceServiceImpl service) {
        this.insuranceService = service;
    }


    @GetMapping(value = "/insurance", produces = DEFAULT_PRODUCE_MEDIA_TYPE)
    public ResponseEntity<InsuranceSum> getInsuranceSum(@RequestParam(value = "code",required = true) String code, @RequestParam(value = "sum",required = true) BigDecimal sum) {
        LOG.debug(new StringBuilder().append("REST request InsuranceSum ").toString());
        return ResponseEntity.ok(insuranceService.getByCode(code,sum));
    }

}
