package kz.alfabank.creditutils.repository;

import kz.alfabank.creditutils.domain.InsuranceDictioanry;
import kz.alfabank.creditutils.domain.InsuranceSum;
import kz.alfabank.creditutils.domain.Response;

import java.math.BigDecimal;
import java.util.List;

public interface InsuranceRepository {
   // List<InsuranceDictioanry> getByCode(String code);
   InsuranceSum getByCode(String code, BigDecimal sum);
}
