package kz.alfabank.creditutils.service;

import kz.alfabank.creditutils.domain.InsuranceDictioanry;
import kz.alfabank.creditutils.domain.InsuranceSum;
import kz.alfabank.creditutils.domain.Response;
import kz.alfabank.creditutils.repository.InsuranceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class InsuranceServiceImpl implements InsuranceRepository {
    private static final Logger LOG = LoggerFactory.getLogger(InsuranceServiceImpl.class);

    @Autowired
    RestTemplate restTemplate;

    @Value("${kz.alfabank.apiurl}")
    private String url;

    public InsuranceSum getByCode(String code, BigDecimal sum) {
        ResponseEntity<Response> response = restTemplate.getForEntity(url + "/"+code, Response.class);
        List<InsuranceDictioanry> list = response.getBody().getResponse();
        List<InsuranceDictioanry> premium = list.stream()
                .filter(a -> Objects.equals(a.getAttName(), "PREMIUM"))
                .collect(Collectors.toList());
        LOG.debug(new StringBuilder().append("Get premium. premium=").append(premium.get(0).getAttValue()).toString());
        BigDecimal rate = new BigDecimal(premium.get(0).getAttValue());
        BigDecimal result = rate.multiply(sum).divide(BigDecimal.valueOf(100));
        InsuranceSum insuranceSum = new InsuranceSum(result.setScale(2,2));
        LOG.debug(new StringBuilder().append("Insurance sum. sum=").append(insuranceSum).toString());
        return insuranceSum;
    }

}
