package kz.alfabank.creditutils;

import kz.alfabank.creditutils.domain.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class CreditutilsApplication extends SpringBootServletInitializer {

	private static final Logger log = LoggerFactory.getLogger(CreditutilsApplication.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CreditutilsApplication.class);
	}
	public static void main(String[] args) {
		SpringApplication.run(CreditutilsApplication.class, args);
	}
/*
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		return args -> {
			Response response = restTemplate.getForObject(
					"https://vserver035.alfa-bank.kz/bpmocrm/commons_service/get_dict_val_attrs/ABK_POST_CATEGORY/2", Response.class);
			log.info(response.toString());

		};
	}*/
}
