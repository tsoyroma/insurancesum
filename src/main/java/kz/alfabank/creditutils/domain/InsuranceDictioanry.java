package kz.alfabank.creditutils.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class InsuranceDictioanry {
    @JsonProperty("ATT_VALUE")
    private String attValue;
    @JsonProperty("ATT_NAME")
    private String attName;

    public String getAttValue() {
        return attValue;
    }

    public void setAttValue(String attValue) {
        this.attValue = attValue;
    }

    public String getAttName() {
        return attName;
    }

    public void setAttName(String attName) {
        this.attName = attName;
    }

    @Override
    public String toString() {
        return "Insurance{" +
                "att_value='" + attValue+ '\'' +
                ", att_name=" + attName +
                '}';
    }
}
