package kz.alfabank.creditutils.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {

    @JsonProperty("RETCODE")
    private Long retcode;
    @JsonProperty("RESPONSE")
    private List<InsuranceDictioanry> response;

    public  Response(){}

    public Long getRetcode() {
        return retcode;
    }

    public void setRetcode(Long retcode) {
        this.retcode = retcode;
    }

    public List<InsuranceDictioanry> getResponse() {
        return response;
    }

    public void setResponse(List<InsuranceDictioanry> response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return
                "RETCODE=" + retcode + response

                ;
    }
}
