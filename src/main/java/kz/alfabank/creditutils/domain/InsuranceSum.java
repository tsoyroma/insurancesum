package kz.alfabank.creditutils.domain;

import java.math.BigDecimal;

public class InsuranceSum {

    private BigDecimal sum;

    public InsuranceSum(BigDecimal sum) {
        this.sum = sum;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }
}
